module home-bank-api

go 1.16

require (
	github.com/asaskevich/govalidator v0.0.0-20210307081110-f21760c49a8d // indirect
	github.com/chai2010/webp v1.1.0
	github.com/disintegration/imaging v1.6.2
	github.com/go-kit/kit v0.12.0
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible
	github.com/google/uuid v1.1.2
	github.com/jackc/pgx/v4 v4.13.0
	github.com/minio/minio-go/v7 v7.0.14
	gopkg.in/yaml.v2 v2.4.0
)
