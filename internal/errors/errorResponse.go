package errors

type ErrorResponse struct {
	Status 	int			`json:"status"`
	Message	string		`json:"message"`
	Details	interface{}	`json:"detail,omitempty"`
}
