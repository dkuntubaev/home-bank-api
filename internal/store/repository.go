package repository

import (
	"context"

	"home-bank-api/internal/model"
)

// Bucket repository interface
type BucketRepository interface {
	CheckIsBucketExist(ctx context.Context, bucketName string) (bool, error)
	CreateBucket(ctx context.Context, bucketName string) error
}

// Image repository interface
type ImageRepository interface {
	UploadImages(ctx context.Context, bucketName string, images *[]model.Image) error
	DeleteImage(ctx context.Context, bucketName, imageID string) error
}
