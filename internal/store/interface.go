package repository

// Main S3 store interface
type S3Store interface {
	Bucket() BucketRepository
	Image() ImageRepository
}

type PostgreSQLStore interface {

}