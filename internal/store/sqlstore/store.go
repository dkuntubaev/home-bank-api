package sqlstore

import (
	"context"
	"strings"

	"github.com/go-kit/kit/log"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Main store entity
type Store struct {
	db     *pgxpool.Pool
	logger log.Logger
}

// Store constructor
func NewStore(db *pgxpool.Pool, logger log.Logger) (*Store, error) {
	repo := &Store{
		db:     db,
		logger: log.With(logger, "rep", "postgresql"),
	}

	err := repo.migrate()
	if err != nil {
		return nil, err
	}

	return repo, nil
}

// Migration method
func (s *Store) migrate() error {
	for i := 0; i < len(migrations); i++ {
		sql := strings.Replace(migrations[i], "$1", "public", 1)
		_, err := s.db.Exec(context.Background(), sql)
		if err != nil {
			return err
		}
	}

	return nil
}

// Action
//func (s *Store) Action() repository.ActionRepository {
//	if s.ActionRepository != nil {
//		return s.ActionRepository
//	}
//
//	s.ActionRepository = &ActionRepository{
//		store: s,
//	}
//
//	return s.ActionRepository
//}
