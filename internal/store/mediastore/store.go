package mediastore

import (
	"github.com/go-kit/kit/log"
	"github.com/minio/minio-go/v7"
	repository "home-bank-api/internal/store"
)

// Store entity
type Store struct {
	db     *minio.Client
	logger log.Logger

	BucketRepository repository.BucketRepository
	ImageRepository  repository.ImageRepository
}

// Store constructor
func NewStore(db *minio.Client, logger log.Logger) *Store {
	return &Store{
		db:     db,
		logger: log.With(logger, "rep", "minio"),
	}
}

// Bucket
func (s *Store) Bucket() repository.BucketRepository {
	if s.BucketRepository != nil {
		return s.BucketRepository
	}

	s.BucketRepository = &BucketRepository{
		store: s,
	}

	return s.BucketRepository
}

// Image
func (s *Store) Image() repository.ImageRepository {
	if s.ImageRepository != nil {
		return s.ImageRepository
	}

	s.ImageRepository = &ImageRepository{
		store: s,
	}

	return s.ImageRepository
}