package mediastore

import (
	"context"

	"github.com/minio/minio-go/v7"
)

// Bucket repository
type BucketRepository struct {
	store *Store
}

// Check bucket existence repository method
func (r *BucketRepository) CheckIsBucketExist(ctx context.Context, bucketName string) (bool, error) {
	return r.store.db.BucketExists(ctx, bucketName)
}

// Create bucket repository method
func (r *BucketRepository) CreateBucket(ctx context.Context, bucketName string) error {
	err := r.store.db.MakeBucket(ctx, bucketName, minio.MakeBucketOptions{})
	if err != nil {
		return err
	}

	return nil
}
