package mediastore

import (
	"bytes"
	"context"
	"encoding/binary"
	"home-bank-api/internal/model"

	"github.com/minio/minio-go/v7"
)

// Image repository constants
const imagesPrefix = "images"

// Image repository
type ImageRepository struct {
	store *Store
}

// Upload images repository method
func (r *ImageRepository) UploadImages(ctx context.Context, bucketName string, images *[]model.Image) error {
	imagesArr := *images
	errChan := make(chan error, 2)
	doneChan := make(chan bool, 2)

	for i := 0; i < len(imagesArr); i++ {
		go func(i int) {
			image := imagesArr[i]
			_, err := r.store.db.PutObject(
				ctx,
				bucketName,
				imagesPrefix+"/"+image.ID+"/"+image.Title,
				bytes.NewReader(image.Data),
				int64(binary.Size(image.Data)),
				minio.PutObjectOptions{
					ContentType: image.ContentType,
				})
			if err != nil {
				errChan <- err
			}
			doneChan <- true
		}(i)
	}

	for i := 0; i < len(imagesArr); i++ {
		select {
		case <-ctx.Done():
			return nil
		case err := <-errChan:
			return err
		case <-doneChan:
			continue
		}
	}

	return nil
}

// Delete image repository method
func (r *ImageRepository) DeleteImage(ctx context.Context, bucketName, imageID string) error {
	errChan := make(chan error, 2)
	doneChan := make(chan bool, 2)
	objectsChan := make(chan minio.ObjectInfo)

	go func() {
		defer close(objectsChan)
		for object := range r.store.db.ListObjects(ctx, bucketName, minio.ListObjectsOptions{
			Prefix:    imagesPrefix + "/" + imageID,
			Recursive: true,
		}) {
			if object.Err != nil {
				errChan <- object.Err
				return
			}
			objectsChan <- object
		}
	}()

	go func() {
		for err := range r.store.db.RemoveObjects(ctx, bucketName, objectsChan, minio.RemoveObjectsOptions{}) {
			errChan <- err.Err
		}
		doneChan <- true
	}()

	for {
		select {
		case <-ctx.Done():
			return nil
		case <-doneChan:
			return nil
		case err := <-errChan:
			return err
		}
	}
}
