package config

import (
	validation "github.com/go-ozzo/ozzo-validation"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
)

const (
	defaultServerPort 		= "8080"
	defaultLogLevel 		= "info"
	defaultDatabaseUsername = ""
	defaultDatabasePassword = ""
	defaultDatabaseHost		= ""
	defaultDatabasePort		= ""
	defaultDatabase			= ""

)

type Config struct {
	ServerPort	string			`yaml:"bind_addr"`
	//LogLevel	string			`yaml:"log_level"`
	Database	DatabaseConfig	`yaml:"database_url"`
	Media		MediaConfig		`yaml:"media_config"`
}

type MediaConfig struct {
	Endpoint		string	`yaml:"endpoint"`
	AcessKeyID		string	`yaml:"acess_key_id"`
	SecretAcessKey	string	`yaml:"secret_acess_key"`
}

type DatabaseConfig struct {
	Username	string		`yaml:"username"`
	Password	string		`yaml:"password"`
	Host		string		`yaml:"host"`
	Port		string		`yaml:"port"`
	Database	string		`yaml:"database"`
}

func (c *Config) Validate() error {
	return validation.ValidateStruct(c,
		validation.Field(c.Media.Endpoint, 		 validation.Required),
		validation.Field(c.Media.AcessKeyID, 	 validation.Required),
		validation.Field(c.Media.SecretAcessKey, validation.Required),
		)
}

func Load(file string, logger log.Logger) (*Config, error) {
	c :=&Config{
		ServerPort: defaultServerPort,
		Database: DatabaseConfig{
			Username: 	defaultDatabaseUsername,
			Password: 	defaultDatabasePassword,
			Host: 		defaultDatabaseHost,
			Port: 		defaultDatabasePort,
			Database: 	defaultDatabase,
		},
	}

	bytes, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	if err = yaml.Unmarshal(bytes, c); err != nil {
		return nil, err
	}
	if err = c.Validate(); err != nil {
		return nil, err
	}
	return c, nil
}


