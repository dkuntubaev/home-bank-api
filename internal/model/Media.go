package model

type Media struct {
	Id		string	`json:"id"`
	Image	[]byte	`json:"image"`
}
