package model

type Merchant struct {
	Id 			int		`json:"id"`
	Name		string	`json:"name"`
	Description	string	`json:"description"`
	ShopList	[]Shop	`json:"shop_list"`
}