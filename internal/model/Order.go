package model

type Order struct {
	Id				int
	RemainingDept	float64
	DeptSeilling	float64
	ItemList		[]Item	`json:"item_list"`
}
