package model

type Client struct {
	Id				int		`json:"id"`
	IIN				string	`json:"iin"`
	Name 			string	`json:"name"`
	Surname 		string	`json:"surname"`
	Phone			string	`json:"phone"`
	AvailableSum	float64	`json:"available_sum"`
	Orders			[]Order	`json:"orders"`
	// Updated_at		time.Time
}
