package model

type Item struct {
	Id			int
	BarCode		string
	Name		string
	Description	string
	Price		float64
	MediaList	[]Media
}
