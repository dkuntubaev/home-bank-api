package model

import (
	"bytes"
	"context"
	"mime"
	"strconv"

	"github.com/chai2010/webp"
	"github.com/disintegration/imaging"
	"github.com/google/uuid"
)

// Main image entity
type Image struct {
	ID          string `json:"id"`
	SourceName  string `json:"source_name"`
	Title       string `json:"title"`
	Data        []byte `json:"data"`
	ContentType string `json:"content_type"`
}

// Image init func
func (im *Image) Init() error {
	uid, err := uuid.NewUUID()
	if err != nil {
		return err
	}

	im.ID = uid.String()
	im.Title = "image"

	return nil
}

// Add image extension to title
func (im *Image) AddExtension() error {
	ext, err := mime.ExtensionsByType(im.ContentType)
	if err != nil {
		return err
	}

	im.Title += ext[len(ext)-1]

	return nil
}

// Image webp converter
func (im *Image) WebPConverter() (*Image, error) {
	buffer := bytes.NewBuffer([]byte{})
	reader := bytes.NewReader(im.Data)

	img, err := imaging.Decode(reader)
	if err != nil {
		return nil, err
	}

	err = webp.Encode(buffer, img, &webp.Options{Lossless: true})
	if err != nil {
		return nil, err
	}

	webpImg := &Image{
		ID:          im.ID,
		SourceName:  im.SourceName,
		Title:       im.Title,
		Data:        buffer.Bytes(),
		ContentType: "image/webp",
	}

	return webpImg, nil
}

// Image resizer func
func (im *Image) Resizer(ctx context.Context) (*[]Image, error) {
	return nil, nil
}

func (im *Image) resize() (*Image, error) {
	reader := bytes.NewReader(im.Data)

	img, err := imaging.Decode(reader)
	if err != nil {
		return nil, err
	}

	resized := imaging.Fill(img, 500, 500, imaging.Center, imaging.Linear)

	ext, err := mime.ExtensionsByType(im.ContentType)
	if err != nil {
		return nil, err
	}

	format, err := imaging.FormatFromExtension(ext[len(ext)-1])
	if err != nil {
		return nil, err
	}

	buffer := bytes.NewBuffer([]byte{})
	err = imaging.Encode(buffer, resized, format)
	if err != nil {
		return nil, err
	}

	title := im.Title + "-" +
		strconv.Itoa(500) + "x" +
		strconv.Itoa(500)

	return &Image{
		ID:          im.ID,
		SourceName:  im.SourceName,
		Title:       title,
		Data:        buffer.Bytes(),
		ContentType: im.ContentType,
	}, nil
}
