package model

type Shop struct {
	Id 			int		`json:"id"`
	Name 		string	`json:"name"`
	Description string	`json:"description"`
	Longitude	float32	`json:"longitude"`
	Latitude	float32	`json:"latitude"`
	ItemList	[]Item	`json:"item_list"`
}
